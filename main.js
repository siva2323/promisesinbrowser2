const container=document.getElementById("container");

fetch("https://jsonplaceholder.typicode.com/users")
    .then(usersDataUnconverted => usersDataUnconverted.json())
    .then(usersData => {
        return Promise.all([usersData,fetch("https://jsonplaceholder.typicode.com/todos")])
    }).then(([usersData,todosResponse]) =>{
        return Promise.all([usersData,todosResponse.json()])
    }).then(([usersData,todosData]) => {
        render([usersData,todosData])
    }).catch(error => {
        console.error("Error occured "+error)
        console.error(error.stack);   
    })


